# configurapi-runner-sns-sqs

This project handles Amazon Web Services (AWS) Simple Notification Service (SNS) subscriptions to a Simple Queue Service (SQS) subscription target.

```
"dependencies": {
    "configurapi": "^1.4.6",
    "configurapi-handler-logging": "^1.3.6",
    "one-liner": "^1.0.3",
    "sqs-consumer": "^3.7.0",
    "sqs-producer": "^1.5.0",
    "uuid": "^3.1.0"
}
```
