import { assert } from "chai"
import { LambdaSqsAdapter } from "../src/LambdaSqsAdapter"
import { IS3EventRecord } from "../src/interfaces/iS3EventRecord"


describe('LambdaSqsAdapter', () =>
{
    const data = {
        messageId: 'b98bad21-144a-44a2-4398-658dd1243063',
        receiptHandle: 'aVeryLongString',
        body: '{"Records":[{"eventVersion":"2.1","eventSource":"aws:s3","awsRegion":"us-east-1","eventTime":"2023-04-01T21:00:00.000Z","eventName":"ObjectCreated:Put","userIdentity":{"principalId":"AWS:AROAUEIVYEWH1232HY663:aUserIdentity"},"requestParameters":{"sourceIPAddress":"10.10.1.2"},"responseElements":{"x-amz-request-id":"VY83J7G83BD621V0","x-amz-id-2":"notSureWhatThisStringIs"},"s3":{"s3SchemaVersion":"1.0","configurationId":"80089188-d080-47c5-ba35-8e44ac56540a","bucket":{"name":"theBucketName-qiwd92xppgg2","ownerIdentity":{"principalId":"A3LK9364AB9TKK"},"arn":"arn:aws:s3:::theBucketName-qiwd92xppgg2"},"object":{"key":"theKeyGoesHere.txt","size":532,"eTag":"446f61518374bcd7153d4360f67fd2f5","sequencer":"008751E1DD09E7B7BB"}}}]}',
        attributes: [],
        messageAttributes: {},
        md5OfBody: 'notValidated',
        eventSource: 'aws:sqs',
        eventSourceARN: 'arn:aws:sqs:us-east-1:123456789012:example-queue-tzlYC2R8atBW',
        awsRegion: 'us-east-1'
    }


    it('toRequest', async () =>
    {
        let request = await LambdaSqsAdapter.toRequest(data)

        assert.strictEqual(request.method, "");
        assert.isObject(request.headers);
        assert.lengthOf(Object.keys(request.headers), 0);
        assert.strictEqual(request.name, "sns_sqs_event")

        assert.isObject(request.query);
        assert.lengthOf(Object.keys(request.query), 0);
        
        assert.isDefined(request.payload);
        assert.isArray(request.payload);
        assert.lengthOf(request.payload, 1);

        assert.strictEqual((<IS3EventRecord[]>request.payload)[0].awsRegion, "us-east-1")
        

    })

})
