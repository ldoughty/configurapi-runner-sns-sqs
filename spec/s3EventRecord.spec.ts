import { assert } from "chai";
import {S3EventRecord} from "../src/entities/s3EventRecord"

describe('s3EventRecord', () =>
{
    it('constructor', () => {
        let input = {
            "eventVersion": "2.1",
            "eventSource": "aws:s3",
            "awsRegion": "us-east-1", "eventTime": "2023-04-01T21:00:00.000Z", "eventName": "ObjectCreated:Put",
            "userIdentity": { "principalId": "AWS:AROAUEIVYEWH1232HY663:aUserIdentity" },
            "requestParameters": { "sourceIPAddress": "10.10.1.2" },
            "responseElements": { "x-amz-request-id": "VY83J7G83BD621V0", "x-amz-id-2": "notSureWhatThisStringIs" },
            "s3": {
                "s3SchemaVersion": "1.0",
                "configurationId": "80089188-d080-47c5-ba35-8e44ac56540a",
                "bucket": {
                    "name": "theBucketName-qiwd92xppgg2",
                    "ownerIdentity": {
                        "principalId": "A3LK9876453TKK"
                    },
                    "arn": "arn:aws:s3:::theBucketName-qiwd92xppgg2"
                },
                "object": {
                    "key": "theKeyGoesHere.txt",
                    "size": 532,
                    "eTag": "446f61518374bcd7153d4360f67fd2f5",
                    "sequencer": "008751E1DD09E7B7BB"
                }
            }
        }
        let record = new S3EventRecord(input)
        
        assert.strictEqual(record.eventVersion, "2.1")
        assert.strictEqual(record.eventSource, "aws:s3")
        assert.strictEqual(record.awsRegion, "us-east-1")
        assert.strictEqual(record.eventTime, "2023-04-01T21:00:00.000Z")
        assert.strictEqual(record.eventName, "ObjectCreated:Put")
        assert.strictEqual(record.s3.s3SchemaVersion, "1.0")
        assert.strictEqual(record.s3.configurationId, "80089188-d080-47c5-ba35-8e44ac56540a")
        assert.strictEqual(record.s3.bucket.name, "theBucketName-qiwd92xppgg2")
        assert.strictEqual(record.s3.bucket.ownerIdentity.principalId, "A3LK9876453TKK")
        assert.strictEqual(record.s3.bucket.arn, "arn:aws:s3:::theBucketName-qiwd92xppgg2")
        assert.strictEqual(record.s3.object.key, "theKeyGoesHere.txt")
        assert.strictEqual(record.s3.object.size, 532)
        assert.strictEqual(record.s3.object.eTag, "446f61518374bcd7153d4360f67fd2f5")
        assert.strictEqual(record.s3.object.sequencer, "008751E1DD09E7B7BB")
    });
});