export interface IS3EventRecord
{
    eventVersion: string;
    eventSource: string;
    awsRegion: string;
    eventTime: string;
    eventName: string;
    s3: {
        s3SchemaVersion: string;
        configurationId: string;
        bucket: {
            name: string;
            arn: string;
            ownerIdentity: {
                principalId: string;
            }
        }
        object: {
            key: string;
            size: number;
            eTag: string;
            sequencer: string;
        }
    }
}