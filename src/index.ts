import { Config, Service, Event, ErrorResponse } from 'configurapi';
import { LambdaSqsAdapter } from './LambdaSqsAdapter';

const oneliner = require('one-liner');

///////////////////////////////////
// Utility functions
///////////////////////////////////
function log(prefix, str)
{
    if(process.env.LOG_LEVEL==='none') return;
    
    let time = new Date().toISOString();
    process.stdout.write(`${time} ${prefix}: ${oneliner(str)}\n`); //Use process.stdout.write() to exclude "$date $awsRequestId" prefix in CloudWatch logs
}


///////////////////////////////////
// Start service
///////////////////////////////////
const config = {
    errorQueueUrl: process.env.CONFIGURAPI_SQS_ERROR_QUEUE,
    responseQueueUrl: process.env.CONFIGURAPI_SQS_RESPONSE_QUEUE
};

const configFile = Config.load('./config.yaml');
const service = new Service(configFile);

service.on("trace", (s) => log('Trace', s));
service.on("debug", (s) => log('Debug', s));
service.on("error", (s) => log('Error', s));

///////////////////////////////////
// Entry point
///////////////////////////////////
exports.handler = async (event, context, callback) => 
{
    try
    {
        let promises = [];

        for(let record of event.Records)
        {
            promises.push(processMessage(record, context));
        }

        await Promise.all(promises);

        return callback(null);
    }
    catch(e)
    {
        return callback(e);
    }
};

async function processMessage(incomingMessage, context) 
{
    try {
        let event = new Event(await LambdaSqsAdapter.toRequest(incomingMessage));
        event.id = context.awsRequestId;

        let logWithEvent = (logLevel, event, message) => log(logLevel, `${event ? event.id : ''} - ${event ? event.correlationId : ''} - ${typeof message === 'string' ? message : JSON.stringify(message)}`);

        console.log = (message) => logWithEvent('Trace', event, message);
        console.error = (message) => logWithEvent('Error', event, message);
        console.warn = (message) => logWithEvent('Warn', event, message);

        await service.process(event);
        await LambdaSqsAdapter.write(event.id, event.response, config);
    }
    catch (error) {
        let response = new ErrorResponse(error, error instanceof SyntaxError ? 400 : 500);

        try
        {
            await LambdaSqsAdapter.write('', response, config);
        }
        catch(err)
        {
            try
            {
                log("error", JSON.stringify(err));
            }
            catch(errorFromListner)
            {
                log("error", errorFromListner);
            }
        }
    }
}