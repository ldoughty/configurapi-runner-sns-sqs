import { ErrorResponse, IResponse, Request } from "configurapi";
import { S3EventRecord } from "./entities/s3EventRecord";

const SqsProducer = require('sqs-producer');
const { v4: uuid } = require('uuid');

function enqueue(queueUrl: string, data)
{
    let queue = SqsProducer.create({ queueUrl: queueUrl });

    let msg = {
        id: uuid(),
        body: JSON.stringify(data)
    };

    return new Promise((resolve, reject) => 
    {
        queue.send([msg], (err) =>
        {
            if (err) return reject(err);
            resolve(undefined);
        });
    });
}

export class LambdaSqsAdapter
{
    static async write(eventId: string, response: Partial<IResponse>, config: { errorQueueUrl: string; responseQueueUrl: string; })
    {
        if (response instanceof ErrorResponse && config.errorQueueUrl)
        {
            return await LambdaSqsAdapter.writeError(eventId, response, config);
        }

        if (config.responseQueueUrl)
        {
            return await LambdaSqsAdapter.writeResponse(eventId, response, config);
        }
    }
    
    static async writeError(eventId: string, response: Partial<IResponse>, config: { errorQueueUrl: string; responseQueueUrl: string; })
    {
        await enqueue(config.errorQueueUrl, {
            'name': 'error',
            'payload': response
        });
    }

    static async writeResponse(eventId: string, response: Partial<IResponse>, config: { errorQueueUrl: string; responseQueueUrl: string; })
    {
        let body;
        if (response.body instanceof String)
        {
            body = response.body;
        }
        else if (response.body instanceof Object)
        {
            let jsonReplacer : any = 'jsonReplacer' in response ? response.jsonReplacer : undefined;

            body = JSON.stringify(response.body, jsonReplacer);
        }
        else if (response.body instanceof Number)
        {
            body = response.body + '';
        }
        else 
        {
            body = response.body;
        }

        await enqueue(config.responseQueueUrl, {
            'name': 'response',
            'payload': response
        });
    }

    static async toRequest(incomingMessage:any)
    {
        let request = new Request(undefined);

        let data = JSON.parse(incomingMessage.body);

        request.method = '';
        request.headers = Object.assign(Object.assign(Object.assign({}, incomingMessage.approximateReceiveCount), incomingMessage.messageAttributes), data.headers || {});
        request.name = data.name || "sns_sqs_event";
        request.query = data.query || {};
        request.payload = data.Records?.map((r) => new S3EventRecord(r)) || undefined;

        return request;
    }
}
