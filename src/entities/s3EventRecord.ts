import { IS3EventRecord } from "../interfaces/iS3EventRecord";

export class S3EventRecord implements IS3EventRecord
{
    eventVersion: string;
    eventSource: string;
    awsRegion: string;
    eventTime: string;
    eventName: string;
    s3: {
        s3SchemaVersion: string;
        configurationId: string;
        bucket: {
            name: string;
            arn: string;
            ownerIdentity: {
                principalId: string;
            }
        }
        object: {
            key: string;
            size: number;
            eTag: string;
            sequencer: string;
        }
    }
    
    constructor(record:IS3EventRecord)
    {
        this.eventVersion = record.eventVersion;
        this.eventSource = record.eventSource;
        this.awsRegion = record.awsRegion;
        this.eventTime = record.eventTime;
        this.eventName = record.eventName;
        this.s3 = record.s3;
    }
}